from turtle import Turtle, Screen

FONT_SIZE = 50

FONT = ("Arial", FONT_SIZE, "normal")

yertle = Turtle()

# The turtle starts life in the center of the window
# but let's assume we've been drawing other things
# and now need to return to the center of the window

yertle.penup()
yertle.home()

# By default, the text prints too high unless we roughly
# readjust the baseline, you can confirm text placement
# by doing yertle.dot() after yertle.home() to see center

yertle.sety(50)

yertle.write("I AM HERE", align="center", font=FONT)

yertle.hideturtle()

screen = Screen()
screen.exitonclick()
