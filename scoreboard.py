from turtle import Turtle
ALIGNMENT = 'center'
FONT_SIZE = 15
FONT = ("Arial", FONT_SIZE, "normal")


class Scoreboard(Turtle):

    def __init__(self):
        super().__init__()
        self.score = 0
        self.high_score = self.data_scoreboard()
        self.penup()
        self.hideturtle()
        self.color('white')
        self.goto(0, 270)
        self.write(f'Score = {self.score}', align=ALIGNMENT, font=FONT)
        self.update_scoreboard()

    def increase_score(self):
        self.score += 1
        self.clear()
        self.update_scoreboard()

    def update_scoreboard(self):
        self.clear()
        self.write(f'Score = {self.score} High score: {self.high_score}', align=ALIGNMENT, font=FONT)

    def reset(self):
        if self.score > int(self.high_score):
            self.high_score = self.score
            with open('data.txt', 'w') as h:
                h.write(str(self.high_score))
        self.score = 0
        self.update_scoreboard()

    def data_scoreboard(self):
        with open('data.txt') as data_score:
            return data_score.read()


